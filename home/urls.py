from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name="index"),
    path('add/', views.add, name='add'),
    path('detail/<int:pk>/', views.detail, name='detail'),
    path('delete/<int:pk>/', views.delete, name='delete'),
] 

