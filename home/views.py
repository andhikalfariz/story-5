from django.shortcuts import render, redirect
from .forms import MatkulForm
from django.http import HttpResponse
from django.shortcuts import redirect
from django.utils import timezone
from django.shortcuts import get_list_or_404, get_object_or_404
from django.contrib import messages
from .models import Matkul
from datetime import datetime

# Create your views here.

def index(request):
    matkuls = Matkul.objects.all()
    context = {'matkuls': matkuls}
    return render(request, 'home/index.html', context)


''' Membuat matkul baru '''
def add(request):
    form = MatkulForm(request.POST or None)
    print(request.POST)
    print(request.method)
    if request.method == 'POST':
        print("alhamdulillah masuk post")
        if form.is_valid():
            form.save()
            return(redirect('home:add'))
    context = {
        'Form': form
    }
    return render(request, 'home/add.html', context)


def matkul_edit(request, pk):
    matkul = get_object_or_404(matkul, pk=pk)
    if request.method == "POST":
        form = MatkulForm(request.POST, instance=matkul)
        if form.is_valid():
            matkul = form.save(commit=False)
            matkul.author = request.user
            matkul.published_date = timezone.now()
            matkul.save()
            return redirect('matkul_detail', pk=matkul.pk)
    else:
        form = MatkulForm(instance=matkul)
    return render(request, 'home/matkul-edit.html', {'form': form})

def detail(request, pk):
    matkul = Matkul.objects.get(id=pk)
    context = {
        'matkul': matkul, 
        }
    return render(request, 'home/matkul-detail.html', context)


def delete(request, pk):
    matkul = Matkul.objects.get(id=pk)
    matkul.delete()
    messages.success(request, (f"Mata Kuliah {matkul.nama_matkul} berhasil dihapus"))
    return render(request, 'home/index.html')

