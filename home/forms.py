from django import forms
from .models import Matkul

class MatkulForm(forms.ModelForm):
    
    class Meta:
        model = Matkul
        fields = ('nama_matkul', 'dosen', 'sks', 'deskripsi', 'semester', 'kelas',) 

    widgets = {
        'nama_matkul' : forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Nama mata kuliah'
            }
        ),

        'dosen' : forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Nama dosen'
            }
        ),

        'sks' : forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Jumlah SKS'
            }
        ),

        'deskripsi' : forms.Textarea(
            attrs={
                'class': 'form-control',
                'placeholder': 'deskripsi'
            }
        ),

        'semester' : forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Semester'
            }
        ),

        'kelas' : forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'kelas'
            }
        ),
    }
