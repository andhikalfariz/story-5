from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

class Matkul(models.Model):
    nama_matkul = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    sks = models.IntegerField()
    deskripsi = models.CharField(max_length=50)
    semester = models.CharField(max_length=50)
    kelas = models.CharField(max_length=10)
    auto_now_add = True #timestamp

    def __str__(self) :
        return "{}. {}".format(self.id, self.nama_matkul)
